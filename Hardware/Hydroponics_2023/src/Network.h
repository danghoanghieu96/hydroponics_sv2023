#include <Config.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

class Network {
    private:
        void reconnectWifi();
        void reconnectMQTT();
    public:
        Network();
        void initWifi();
        void initMQTT();
        void publishData();
};